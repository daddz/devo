<p align="center">
<a href="https://pypi.org/project/devo/">
    <img src="https://badge.fury.io/py/devo.svg" alt="Package version">
</a>
</p>

---

# devo

devo is a small, highly opinionated command line tool and project template for web-based python applications.

It generates projects with the following features:
* kustomize based Kubernetes templates
* skaffold.yaml template
* GitLab CI template
* optional database support
* GitLab CI variable syncing
* integrate with Kubernetes cluster for test and deployment

## Requirements

### Local development with devo

* [Python 3.6+](https://www.python.org/)
* [skaffold 0.30](https://skaffold.dev/)
* [minikube 1.1.1](https://kubernetes.io/docs/tasks/tools/install-minikube/)
* [kubectl 1.14.3](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* [kustomize 2.0.3](https://github.com/kubernetes-sigs/kustomize/releases)

### Automated testing and deployment
* GitLab 11.11
* GitLab account
* K8s cluster 


## Installation
```shell
$ pip3 install devo
```

## Example

### Generate new project
```shell
$ devo create web my-project
Starting project my-project
Local Git user name [<global git username>]: <project specific username>
Local Git email [<global git email>]: <project specific email>
GitLab Server [https://gitlab.com]: <custom gitlab server>
GitLab User: my-username
GitLab Token: my-token
Checking credentials for https://gitlab.com
Authorization succeeded for https://gitlab.com
GitLab Group Name: my-group
Checking group availability for my-group
Found existing group
GitLab Project Name [my-project]:
Creating project my-project on https://gitlab.com
Project created
Use GitLab Docker registry [Y/n]:
Registry URL [registry.gitlab.com]:
Docker Image Name [registry.gitlab.com/my-group/my-project]:
k8s Cluster URL: https://my.own.k8s.cluster
k8s User: my-k8s-username
k8s Token: my-k8s-token
Project needs database [y/N]: y
Generating project folder my-project
Switching into my-project
Generating .gitlab-ci.yml
Generating k8s templates
Generating skaffold.yaml
Syncing CI variables
Initializing Git
Initialized empty Git repository in ./my-project/.git/
```

### Local development
```shell
$ minikube start
$ cd my-project
$ skaffold dev
```

